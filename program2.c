#include <stdio.h> 
#include <stdlib.h>
#include <ctype.h>
#include <string.h> 

int main(int argc, char *argv[] ) {
    
    if( argc != 3) { //command line check
        printf("Must supply a src and dest filename\n");
        exit(1);
    }
    
    FILE *src; //open and check source file
    src = fopen(argv[1], "r");
    if(!src) {
        printf("Can't open source file for reading");
        exit(1);
    }
    
    FILE *dest; //open and check dest file
    dest = fopen(argv[2], "w");
    if(!dest){
        printf("Can't open dest file for reading\n");
        exit(1);
    }

    char line[1000]; //input from source file
    int j =0;
    while(fgets(line,1000,src) != NULL ) {
        
        char newline[1000]; //
        int i = strlen(line) -1;
        printf("%d \n", i);
        int i2 = 0;
        
        while (line[i] != '\0')  {
            newline[i2] = line[i];
            i--;    
            i2++;
        }
        
        
        newline[i] = '\0';
        if(newline[0] == '\n' && j == 0 ) {
            for(int i =0; i < strlen(newline); i++) {
                newline[i] = newline[i+1];
            }
            j++;
        }
        fprintf(dest, "%s", newline);
        
    }



    
    fclose(src);
    fclose(dest);
    
}